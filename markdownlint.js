const markdownlint = require("markdownlint");
var path = require('path'), fs=require('fs');
const util = require('util')

var files = [];

function fromDir(startPath,filter,callback) {
    //console.log('Starting from dir '+startPath+'/');
    if (!fs.existsSync(startPath)){
        console.log("no dir ",startPath);
        return;
    }

    var files=fs.readdirSync(startPath);
    for(var i=0;i<files.length;i++){
        var filename=path.join(startPath,files[i]);
        var stat = fs.lstatSync(filename);
        if (stat.isDirectory()){
            fromDir(filename,filter,callback); //recurse
        }
        else if (filter.test(filename)) callback(filename);
    }
}

fromDir('.',/\.md$/,function(filename) {
    // Add filename to array
    files.push(filename);
});

const options = {
  "files": files,
  "config": require("/usr/lib/git_utils/style/relaxed.json")
};

markdownlint(options, function callback(err, result) {
  if (!err) {
    var failures = false
    Object.keys(result).forEach(function(key) {
      var val = result[key]
      if (val === undefined || val.length == 0) {
        console.log("[pass] " + key)
      } else {
        failures = true // fail this script if at least one markdown file has findings
        console.log("[fail] " + key)
        console.log(util.inspect(val, false, null, true /* enable colors */))
      }
    });
    if (failures) {
      process.exit(1);
    }
  } else {
    console.log("There was an error running markdownlint.");
    process.exit(1);
  }
});
