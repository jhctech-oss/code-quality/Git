## Build for Git Code Quality

## specify your base image pulled from docker hub
FROM alpine:latest

## specify the group or individual maintaining image
LABEL maintainer="Mike Atkinson"

RUN apk update && apk upgrade && apk add --no-cache --update \
  bash \
  git \
  python3 \
  python3-dev \
  npm \
&& rm -rf /var/lib/apt/lists/*

RUN npm install markdownlint --save-dev

COPY markdownlint.js /usr/lib/git_utils/markdownlint.js
COPY style /usr/lib/git_utils/style
